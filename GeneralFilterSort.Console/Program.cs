﻿using GeneralFilterSort.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFilterSort.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            UserModel[] users = Utilities.SeedUsers();
            Utilities.ShowUsers(users);
            System.Console.WriteLine("=================================");
            users = SortDynamic<UserModel>.Sort(users, new string[] { "Email", "Password", "Username" }, new bool[] { false, false, false });
            Utilities.ShowUsers(users);
            System.Console.WriteLine("=================================");
            users = FilterDynamic<UserModel>.FilterByContains(users, new string[] { "Email", "Username", "DateOfBirth" }, new string[] { "cc", "bb", "199" });
            Utilities.ShowUsers(users);
        }
    }
}
