General Sort And Filter in C# using Linq
===
This project contains a class library with methods to sort and filter an array of objects based on some parameters. 

The general idea was to have a method that combines filter and sort parameters, and bring the number of parameters to a minimum, along with packaging it as simple and as readable as possible.

The sort method takes 3 parameters:
- The array that needs to be sorted
- A list of names of the properties that the array is being sorted by
- A list of boolean values that indicate whether or not the array should be sorted descending for the property on the index of the boolean value

Example pseudocode: 

	xyz = Library.sort(xyz, {"Username", "Password"}, {true, false});

This will sort the array xyz descending by the Username property, and ascending by the Password property - provided the array contains objects that have those properties.

The filter method takes 3 parameters:
- The array that needs to be filtered
- A list of names of the properties that the list will be filtered by
- A list of values that represent filter criteria for each of the properties

This is basicaly a search were the _contains_ method is used in each .Where() clause

Example pseudocode:

	xyz = Library.filterByContains(xyz, {"Username", "DateOfBirth"}, {"mack", "19"});

This will return an array that contains only items whos _Username_ property contains the string "_mack_" anywhere in it, and whose _DateOfBirth_ property contains a "_19_". **Note**: The filter can be used to filter numeric fields as well as string fields. Use a string parameter _(like the "19" used in the example)_ to filter for people that have a 19 in their date of birth _{19xx, x19x, xx19}_.