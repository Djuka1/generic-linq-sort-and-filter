﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFilterSort.Lib
{
    public class FilterDynamic<TEntity> where TEntity : class
    {
        public static TEntity [] FilterByContains(TEntity [] entities, string [] propertyParams, string [] containsParams)
        {
            Type entityType = typeof(TEntity);
            PropertyInfo propertyInfo = entityType.GetProperty(propertyParams[0]);
            TEntity [] filtered = entities.Where(x => propertyInfo.GetValue(x, null).ToString().Contains(containsParams[0])).ToArray();
            for(int i=1;    i < propertyParams.Length; i++)
            {
                propertyInfo = entityType.GetProperty(propertyParams[i]);
                filtered = filtered.Where(x => propertyInfo.GetValue(x, null).ToString().Contains(containsParams[i])).ToArray();
            }
            return filtered;
        }
    }
}
