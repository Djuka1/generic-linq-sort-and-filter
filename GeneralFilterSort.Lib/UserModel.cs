﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFilterSort.Lib
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int DateOfBirth { get; set; }

        public override string ToString()
        {
            return "{" + $"{nameof(Id)}={Id}, {nameof(Email)}={Email}, {nameof(Password)}={Password}, {nameof(Username)}={Username}, {nameof(DateOfBirth)}={DateOfBirth}" + "}";
        }
    }
}
