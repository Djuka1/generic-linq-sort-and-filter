﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFilterSort.Lib
{
    public static class Utilities
    {
        public static UserModel [] SeedUsers()
        {
            return new UserModel[]
            {
                new UserModel{Id=100, Email="cccc@gmail.com", Password="aaaa",Username="aabb", DateOfBirth=1991},
                new UserModel{Id=200, Email="bbbb@gmail.com", Password="aaaa",Username="aabb", DateOfBirth=1983},
                new UserModel{Id=300, Email="cccc@gmail.com", Password="bbbb",Username="aabb", DateOfBirth=1995},
                new UserModel{Id=400, Email="cccc@gmail.com", Password="bbbb",Username="cccc", DateOfBirth=1994},
                new UserModel{Id=500, Email="aaaa@gmail.com", Password="aaaa",Username="aabb", DateOfBirth=1978},
                new UserModel{Id=600, Email="cccc@gmail.com", Password="bbbb",Username="bbbb", DateOfBirth=1985}
            };
        }

        /* 
                When sorted should look like:
                new UserModel{Email="aaaa@gmail.com", Password="aaaa",Username="aabb"},
                new UserModel{Email="bbbb@gmail.com", Password="aaaa",Username="aabb"},
                new UserModel{Email="cccc@gmail.com", Password="aaaa",Username="aabb"},
                new UserModel{Email="cccc@gmail.com", Password="bbbb",Username="aabb"},
                new UserModel{Email="cccc@gmail.com", Password="bbbb",Username="bbbb"},
                new UserModel{Email="cccc@gmail.com", Password="bbbb",Username="cccc"}
        */

        public static void ShowUsers(UserModel [] users)
        {
            foreach(UserModel user in users)
            {
                Console.WriteLine(user);
                Console.WriteLine("----------------------------------");
            }
        }
    }
}
