﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GeneralFilterSort.Lib
{
    public static class SortDynamic<TEntity> where TEntity : class
    {
        public static TEntity[] Sort(TEntity [] entities, string [] orderByProperties, bool [] descending)
        {
            Type entityType = typeof(TEntity);
            PropertyInfo property = entityType.GetProperty(orderByProperties[0]);
            IOrderedEnumerable<TEntity> orderQuery = entities.OrderBy(x=>property.GetValue(x, null));
            if (descending[0])
            {
                orderQuery = entities.OrderByDescending(x => property.GetValue(x, null));
            }
            for(int i=1;    i < orderByProperties.Length; i++)
            {
                property = entityType.GetProperty(orderByProperties[i]);
                if (descending[i])
                {
                    orderQuery = orderQuery.ThenByDescending(x => property.GetValue(x, null));
                }
                else
                {
                    orderQuery = orderQuery.ThenBy(x => property.GetValue(x, null));
                }
            }
            return orderQuery.ToArray();
        }
    }
}
